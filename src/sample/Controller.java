package sample;

import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("unused")
public class Controller {


    public TextField invoiceAmount;
    public TextField gratuityAmountInRub;
    public TextField gratuityAmount;
    public ToggleGroup toggleCurrencies;
    public ToggleGroup togglePercent;
    public AnchorPane panelOutputInRub;
    public AnchorPane panelCurrencies;
    public MaterialDesignIconView buttonCloseProgram;
    public MaterialDesignIconView buttonCloseCurrencies;
    public Button buttonPay;

    /**
     * Метод, вызывающий цепочку действий при нажатии на кнопку "Оставить чаевые"
     *
     * @param actionEvent событие
     * @throws IOException исключение
     */
    public void onClick(ActionEvent actionEvent) throws IOException {
        if (!invoiceAmount.getText().isEmpty()) {
            RadioButton percent = (RadioButton) togglePercent.getSelectedToggle();
            if (percent == null) {
                showError("Выберите процент из данного списка");
            } else {
                printGratuity(percent.getEllipsisString());
            }
        } else {
            showError("Введите сумму чека");
        }
    }

    /**
     * Метод, который записывает сумму чаевых в текстовые поля
     *
     * @param percent процент чаевых
     * @throws IOException исключение
     */
    private void printGratuity(String percent) throws IOException {
        if (toggleCurrencies.getSelectedToggle() != null) {
            if (!((RadioButton) toggleCurrencies.getSelectedToggle()).getText().equals("рубль")) {
                gratuityAmountInRub.setText(calcGratuityAmountInRub(percent));
            }
            gratuityAmount.setText(calcGratuityAmount(percent));

        } else {
            showError("Выберите валюту из данного списка");
        }
    }

    /**
     * Метод для расчета чаевых в выбранной валюте
     *
     * @param percent процент чаевых
     * @return значение чаевых в выбранной пользователем валюте
     */
    private String calcGratuityAmount(String percent) {
        if (invoiceAmount.getText().trim().matches("\\d*|\\d+[.]\\d*|[.]\\d*")) {
            Double invoice = Double.valueOf(invoiceAmount.getText());
            Double valueOfPerCent = Double.valueOf(percent);
            return String.valueOf(Math.round(invoice * valueOfPerCent));
        } else {
            showError("Введите числовые значения в поле \"Сумма чека\".\n " +
                    "Примеры:\nцелое число: 1234\nвещественное число: 1234.5");
        }
        return "";
    }

    /**
     * Метод для расчета чаевых в рублях
     *
     * @param percent процент чаевых
     * @return значение чаевых в рублях
     * @throws IOException исключение
     */
    private String calcGratuityAmountInRub(String percent) throws IOException {
        if (invoiceAmount.getText().trim().matches("\\d*|\\d+[.]\\d*|[.]\\d*")) {
            Double invoice = Double.valueOf(invoiceAmount.getText());
            Double valueOfPerCent = Double.valueOf(percent);
            Double valueCurrency = Double.valueOf(getValueCurrencyInRub());
            return String.valueOf(Math.round(invoice * valueCurrency * valueOfPerCent));

        } else {
            showError("Введите числовые значения в поле \"Сумма чека\".\n " +
                    "Примеры:\nцелое число: 1234\nвещественное число: 1234.5");
        }
        return "";
    }

    /**
     * Метод, для вывода пользователю сообщение об ошибке
     *
     * @param message сообщение, выводимое на экране
     */
    private void showError(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setTitle("Ошибка");
        alert.setHeaderText("Что-то пошло не так :(");
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Метод, для поиска значения выбранной пользователем валюты в рублях
     *
     * @return значение валюты в рублях
     * @throws IOException исключение
     */
    private String getValueCurrencyInRub() throws IOException {
        RadioButton radioButton = (RadioButton) toggleCurrencies.getSelectedToggle();
        String string = "";
        Pattern pattern = Pattern.compile("<td>" + radioButton.getText() + "</td>" + " {6}<td>\\d+[,]\\d*</td>");
        Matcher matcher = pattern.matcher("");
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src/sample/currenciesTable.txt"))) {
            String temp;
            while ((temp = bufferedReader.readLine()) != null) {
                matcher.reset(temp);
                while (matcher.find()) {
                    string = matcher.group();
                }
            }
        }
        Pattern patternTwo = Pattern.compile("\\d+[,]\\d*");
        Matcher matcherTwo = patternTwo.matcher(string);
        String value = "";
        while (matcherTwo.find()) {
            value = matcherTwo.group().replace(",", ".");
        }
        return value;
    }

    /**
     * Метод для обновления таблицы курса валют с интернета
     *
     * @param actionEvent событие
     * @throws IOException исключение
     */
    public void upgradeCurrencies(ActionEvent actionEvent) throws IOException {
        URL url = new URL("https://www.cbr.ru/currency_base/daily/");
        StringBuilder string = new StringBuilder();
        if (checkInternetConnection(url)) {
            try (LineNumberReader reader = new LineNumberReader(new InputStreamReader(url.openStream()))) {
                String temp = reader.readLine();
                while (temp != null) {
                    string.append(temp);
                    temp = reader.readLine();
                }
            }
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src/sample/currenciesTable.txt"))) {
                bufferedWriter.write(string.toString());
            }
            showSuccess();
        } else {
            showError("Проверьте соединение с интернетом");
        }
    }

    /**
     * Метод, показывающий пользователю сообщение об успехе
     */
    private void showSuccess() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initModality(Modality.NONE);
        alert.setTitle("Успех");
        alert.setHeaderText("Обновление курса валют произошло успешно");
        alert.setContentText("Курс валют обновлен в соответствии текущим значениям");
        alert.show();
    }

    /**
     * Метод, проверящий подключение к интернету
     *
     * @param url ссылка
     * @return true if соеденение с интернетом установлено else false
     */
    private static boolean checkInternetConnection(URL url) {
        try {
            URLConnection connection = url.openConnection();
            if (connection.getInputStream() != null) {
                return true;
            }
        } catch (IOException ignored) {

        }
        return false;
    }

    /**
     * Метод для закрытия окна
     *
     * @param mouseEvent событие, связанное с компьютерной мышью
     */
    public void close(MouseEvent mouseEvent) {
        if (mouseEvent.getSource() == buttonCloseProgram) {
            ((Stage) ((MaterialDesignIconView) mouseEvent.getSource()).getScene().getWindow()).close();
        } else {
            panelCurrencies.setVisible(false);
        }
    }

    /**
     * Метод для сворачивания окна
     *
     * @param mouseEvent событие, связанное с компьютерной мышью
     */
    public void minimize(MouseEvent mouseEvent) {
        ((Stage) ((MaterialDesignIconView) mouseEvent.getSource()).getScene().getWindow()).setIconified(true);
    }

    /**
     * Метод, показывающий панель со списком валют
     *
     * @param actionEvent событие
     */
    public void showPanelCurrencies(ActionEvent actionEvent) {
        panelCurrencies.setVisible(true);
    }

    /**
     * Метод для выбора валюты
     *
     * @param actionEvent событие
     */
    public void selectCurrency(ActionEvent actionEvent) {
        RadioButton radioButton = (RadioButton) actionEvent.getSource();
        if (!radioButton.getText().equals("Рубль")) {
            toChangeVisible(true, false);
        } else {
            toChangeVisible(false, true);
        }
    }

    /**
     * Метод для изменения видимости панели вывода чаевых в рублях
     *
     * @param first  первое значение истинности
     * @param second второе значение истинности
     */
    private void toChangeVisible(boolean first, boolean second) {
        panelOutputInRub.setVisible(first);
        buttonPay.setVisible(second);
    }
}
